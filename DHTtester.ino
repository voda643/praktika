// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor
#include <TroykaMQ.h>
#include <LiquidCrystal.h>
#include "DHT.h"

// имя для пина, к которому подключен датчик
#define PIN_MQ9         A0
// имя для пина, к которому подключен нагреватель датчика
#define PIN_MQ9_HEATER  13
 

#define DHTPIN 2     // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment whatever type you're using!
#define DHTTYPE DHT11 
#define MQ2pin (0)// DHT 11
//#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 3 (on the right) of the sensor to GROUND (if your sensor has 3 pins)
// Connect pin 4 (on the right) of the sensor to GROUND and leave the pin 3 EMPTY (if your sensor has 4 pins)
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
MQ9 mq9(PIN_MQ9, PIN_MQ9_HEATER);
LiquidCrystal lcd(7, 8, 9, 10, 11 , 12);
int pinData = 12;
DHT dht(DHTPIN, DHTTYPE);
float sensorValue;



void setup() {
  // открываем последовательный порт
  Serial.begin(9600);
  // запускаем термоцикл
  // в течении 60 секунд на нагревательный элемент подаётся 5 вольт
  // в течении 90 секунд — 1,5 вольта
  mq9.cycleHeat();


  
  Serial.begin(9600);
  Serial.println(F("DHTxx test!"));

  dht.begin();
  
  Serial.begin(9600); // настроить последовательный порт на скорость 9600
  Serial.println("Gas sensor warming up!");
  delay(20000);       // дать MQ-2 время для прогрева

  
}

void loop() {
  // если прошёл интервал нагрева датчика
  // и калибровка не была совершена
  if (!mq9.isCalibrated() && mq9.atHeatCycleEnd()) {
    // выполняем калибровку датчика на чистом воздухе
    mq9.calibrate();
    // выводим сопротивление датчика в чистом воздухе (Ro) в serial-порт
    Serial.print("Ro = ");
    Serial.println(mq9.getRo());
    // запускаем термоцикл
    mq9.cycleHeat();
  }

  
  sensorValue = analogRead(MQ2pin); // прочитать аналоговый вход 0
  
  Serial.print("Sensor Value: ");
  Serial.print(sensorValue);
  
  if(sensorValue > 300)
  {
    lcd.setCursor(10,0);
    lcd.write("Smoke!");
    Serial.print("Smoke detected!");
  }
   Serial.println("");
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  int h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  int t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  int f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  int hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  int hic = dht.computeHeatIndex(t, h, false);
  lcd.begin(16, 2);
  lcd.setCursor(0,1); 
  lcd.write(("Hum: "));
  lcd.print(h);
  lcd.write(("C"));
    
  lcd.setCursor(0,0); 
  lcd.write(("Temp: "));
  lcd.print(t);
  lcd.write(("C"));
  
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
   // если прошёл интевал нагрева датчика
  // и калибровка была совершена 
  if (mq9.isCalibrated() && mq9.atHeatCycleEnd()) {
    // выводим отношения текущего сопротивление датчика
    // к сопротивлению датчика в чистом воздухе (Rs/Ro)
    Serial.print("Ratio: ");
    Serial.print(mq9.readRatio());
     if (mq9.readRatio() < 5){
      lcd.setCursor(10,1);
      lcd.write("Gas!");
      delay(1000);
      
   }
   
    // выводим значения газов в ppm
    
    Serial.print(" LPG: ");
    Serial.print(mq9.readLPG());
    Serial.print(" ppm ");
    Serial.print(" Methane: ");
    Serial.print(mq9.readMethane());
    Serial.print(" ppm ");
    Serial.print(" CarbonMonoxide: ");
    Serial.print(mq9.readCarbonMonoxide());
    Serial.println(" ppm ");
    delay(100);
    // запускаем термоцикл
    mq9.cycleHeat();
  }
}
